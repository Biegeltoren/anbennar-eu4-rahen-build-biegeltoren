
namespace = rahen

country_event = { #Event to make Rajnadhaga and dwarven adventurers friends
	id = rahen.1
	title = rahen.1.t
	desc = rahen.1.d
	picture = ECONOMY_eventPicture

	is_triggered_only = yes
	
	trigger = {
		province_id = 4266
		owner = { culture_group = dwarven }
	}

	option = {
		name = rahen.1.a
		ai_chance = {
			factor = 1
		}
		
	}
	option = {
		name = rahen.1.b
		ai_chance = {
			factor = 0
		}
		
	}
}
