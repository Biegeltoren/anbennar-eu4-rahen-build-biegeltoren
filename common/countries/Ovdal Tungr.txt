#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 204  117  21 }

revolutionary_colors = { 204  117  21 }

historical_idea_groups = {
	trade_ideas
	defensive_ideas	
	maritime_ideas
	economic_ideas
	offensive_ideas
	expansion_ideas
	quality_ideas	
	influence_ideas
}

historical_units = {
	dwarven_1_shieldwall
	dwarven_5_pepperers
	dwarven_6_ram_cavalry
	dwarven_9_pepper_and_phalanx
	dwarven_10_ram_rangers
	dwarven_12_gun_and_tortoise
	dwarven_15_war_wagons
	dwarven_17_improved_ram_rangers
	dwarven_19_pepper_and_powder
	dwarven_23_dragoons
	dwarven_26_engineers
	dwarven_28_reformed_gun_and_tortoise
	dwarven_28_mecharams
	dwarven_30_repeating_rifles
}
monarch_names = {

	#Generic Dwarf Names
	
	"Adar #0" = 50
	"Adranbur #0" = 50
	"Ad�n #0" = 50
	"Balgar #0" = 50
	"Balgor #0" = 50
	"Balin #0" = 50
	"Bardin #0" = 50
	"Bardur #0" = 50
	"Beld�n #0" = 50
	"Belgar #0" = 50
	"Bofur #0" = 50
	"Bolin #0" = 50
	"Bombur #0" = 50
	"Borin #0" = 50
	"Brann #0" = 50
	"Brok #0" = 50
	"Bruenor #0" = 50
	"Calin #0" = 50
	"C�n #0" = 50
	"Dagran #0" = 50
	"Dain #0" = 50
	"Dalbaran #0" = 50
	"Dalmund #0" = 50
	"Dolin #0" = 50
	"Dorin #0" = 50
	"Dorri #0" = 50
	"Durb�r #0" = 50
	"Durin #0" = 50
	"Dwalin #0" = 50
	"Dwarin #0" = 50
	"Ed�n #0" = 50
	"Falstad #0" = 50
	"Far�n #0" = 50
	"Fimbur #0" = 50
	"Flondi #0" = 50
	"Foldan #0" = 50
	"Gorar #0" = 50
	"Goren #0" = 50
	"Gotrek #0" = 50
	"Grim #0" = 50
	"Grimmar #0" = 50
	"Grimmun #0" = 50
	"Gromar #0" = 50
	"Gronmar #0" = 50
	"Gr�aim #0" = 50
	"Gr�ar #0" = 50
	"Gr�n #0" = 50
	"Gundobad #0" = 50
	"Gundobar #0" = 50
	"Gundobin #0" = 50
	"Gundomar #0" = 50
	"Hard�n #0" = 50
	"Harl�n #0" = 50
	"Harr�k #0" = 50
	"Hjal #0" = 50
	"Hjalmar #0" = 50
	"Karrom #0" = 50
	"Kazad�r #0" = 50
	"Khadan #0" = 50
	"Kildan #0" = 50
	"Kildar #0" = 50
	"Kild�m #0" = 50
	"K�rg�m #0" = 50
	"Lod�n #0" = 50
	"Lorek #0" = 50
	"Lorimbur #0" = 50
	"Madar�n #0" = 50
	"Magar #0" = 50
	"Magni #0" = 50
	"Magnus #0" = 50
	"Mag�n #0" = 50
	"Morzad #0" = 50
	"Muradin #0" = 50
	"Nali #0" = 50
	"Nalin #0" = 50
	"Norb�n #0" = 50
	"Norb�r #0" = 50
	"Nori #0" = 50
	"Norimbur #0" = 50
	"Norin #0" = 50
	"Odar #0" = 50
	"Odur #0" = 50
	"Od�n #0" = 50
	"Okar #0" = 50
	"Olin #0" = 50
	"Orim #0" = 50
	"Orin #0" = 50
	"Ragun #0" = 50
	"Ragund #0" = 50
	"Runmar #0" = 50
	"R�nim�r #0" = 50
	"Sevrund #0" = 50
	"Simb�r #0" = 50
	"Simur #0" = 50
	"Sim�n #0" = 50
	"Sindri #0" = 50
	"Snorri #0" = 50
	"Storin #0" = 50
	"Storlin #0" = 50
	"Storum #0" = 50
	"Thargas #0" = 50
	"Thargor #0" = 50
	"Thinbur #0" = 50
	"Thindobar #0" = 50
	"Thindri #0" = 50
	"Thingrim #0" = 50
	"Thinobad #0" = 50
	"Thorar #0" = 50
	"Thorgrim #0" = 50
	"Thorin #0" = 50
	"Thrain #0" = 50
	"Thrair #0" = 50
	"Thror #0" = 50
	"Thr�n #0" = 50
	"Thr�r #0" = 50
	"Thudri #0" = 50
	"Tungdil #0" = 50
	"Ungrim #0" = 50
	"Urist #0" = 50
	"Ur�r #0" = 50
	
	"Adala #0" = -10
	"Aira #0" = -10
	"Anvi #0" = -10
	"Auci #0" = -10
	"Balga #0" = -10
	"Barila #0" = -10
	"Barra #0" = -10
	"Belga #0" = -10
	"Belgia #0" = -10
	"Bella #0" = -10
	"Branma #0" = -10
	"Brenla #0" = -10
	"Brenleil #0" = -10
	"Brenlel #0" = -10
	"Brina #0" = -10
	"Broga #0" = -10
	"Bronis #0" = -10
	"Cala #0" = -10
	"Dalina #0" = -10
	"Dwalra #0" = -10
	"Edeth #0" = -10
	"Etta #0" = -10
	"Grelba #0" = -10
	"Grimma #0" = -10
	"Groria #0" = -10
	"Gruia #0" = -10
	"Isondi #0" = -10
	"Lahgi #0" = -10
	"Lisban #0" = -10
	"Magda #0" = -10
	"Mistri #0" = -10
	"Moira #0" = -10
	"Mori #0" = -10
	"Moria #0" = -10
	"Naim #0" = -10
	"Ovondi #0" = -10
	"Ronla #0" = -10
	"Sivondi #0" = -10
	"Tharkuni #0" = -10
	"Thohga #0" = -10
	"Ungi #0" = -10
}

leader_names = {

	Battlehammer Axehammer Ettinkiller Minesmiter Minesmith Hammersmith Forgehammer Bloodsteel Earthfist Bronzebeard Goldbeard Stonebeard Hammerbeard Frostbeard Whitebeard
	Breakbeard Ringbeard Gembeard Ogreslayer Orcslayer Spiderslayer Goblinslayer Graybeard Greybeard Redbeard Warhorn Gemhorn Longbeard Truebellow Blackbellow Strongbellow
	Shalesmith Shaleaxe Shalehammer Shaleforger Shalebeard Heavyaxe Skullaxe Gemcraver Goodanvil Blackanvil Shaleanvil Forgeanvil Axeanvil Trueanvil Bloodanvil 
	Thunderfist Thunderhammer Thunderaxe Stormfist Stormhammer Stormaxe Stormanvil Stormbeard Stromanvil Thundersteel Stormsteel Elderbeard Oldbeard Copperbeard Gritaxe
	Flameforge Flameaxe Flamehammer Flamefist Hammerkeeper Stormkeeper Beardfist Hornfist Battlebeard Hardaxe Hardhammer Hammerhand Forgebar Ironbar Goldbar Bronzebar Steelbar
	Ironstar Ironaxe Ironsteel Ironhammer Ironfist Ironhelm
	
}
ship_names = {
	#Generic Cannorian
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear Beaver "Black Galley" "Black Pinnace" "Black Prince" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Elephant Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful President Prince "Prince Consort" "Prince Royal"
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite "Young Prince" Zealous

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor Count Lord Baron

	Knight Paladin Dragonslayer Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
	
	#Elven
	Lunetine Agraseina Estaforra
	
	#Regent Court Deities
	Castellos Dame "The Dame" Halanna Ysh Yshtralania Agradls Adean Esmaryal Ryala Edronias Falah Nerat Ara Minara Munas Moonsinger Nathalyne Begga Corin Balgar
	Uelos Drax'os
}

army_names = {
	"T�ngric Army" "Army of $PROVINCE$"
}

fleet_names = {
	"T�ngric Fleet" "Bronze Squadron" "Steel Squadron" "Iron Squadron" "Copper Squadron" "Gold Squadron"
}