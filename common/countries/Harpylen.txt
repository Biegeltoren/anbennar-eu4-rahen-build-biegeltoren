#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 200  169  168 }

revolutionary_colors = { 200  169  168 }

historical_idea_groups = {
	economic_ideas
	offensive_ideas
	influence_ideas
	defensive_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	harpy_1_harpy_flock
	harpy_6_flock_leads
	harpy_12_shriekers
	harpy_18_flying_rifles
	harpy_23_shriek_chargers
	harpy_26_flying_banshees
	harpy_28_flying_grenadiers
}

monarch_names = {

	#Generic Bulwari Names
	"Aban #0" = 0
	"Abazu #0" = 0
	"Absol #0" = 0
	"Adad #0" = 0
	"Akur #0" = 0
	"Aloros #0" = 0
	"Anshar #0" = 0
	"Arad #0" = 0
	"Arashk #0" = 0
	"Arhab #0" = 0
	"Arsham #0" = 0
	"Arvand #0" = 0
	"Ashan #0" = 0
	"Ashur #0" = 0
	"Asor #0" = 0
	"Atbin #0" = 0
	"Athan #0" = 0
	"Awbal #0" = 0
	"Baraz #0" = 0
	"Barid #0" = 0
	"Barseen #0" = 0
	"Bavar #0" = 0
	"Behlil #0" = 0
	"Berk #0" = 0
	"Bullud #0" = 0
	"Bupalos #0" = 0
	"Cahit #0" = 0
	"Dagan #0" = 0
	"Darab #0" = 0
	"Darian #0" = 0
	"Dariush #0" = 0
	"Dartaxes #0" = 0
	"Demartos #0" = 0
	"Demos #0" = 0
	"Durul #0" = 0
	"Duzan #0" = 0
	"Duzar #0" = 0
	"Duzi #0" = 0
	"Ekrem #0" = 0
	"Erbay #0" = 0
	"Erol #0" = 0
	"Erten #0" = 0
	"Fazil #0" = 0
	"Giz #0" = 0
	"Hazan #0" = 0
	"Hermeias #0" = 0
	"Himmet #0" = 0
	"Huran #0" = 0
	"Husaam #0" = 0
	"Hyaclos #0" = 0
	"Idad #0" = 0
	"Idris #0" = 0
	"Ikelos #0" = 0
	"Irran #0" = 0
	"Iskrates #0" = 0
	"Jahangir #0" = 0
	"Jareer #0" = 0
	"Kalib #0" = 0
	"Kaptan #0" = 0
	"Kartal #0" = 0
	"Kaveh #0" = 0
	"Kikud #0" = 0
	"Korydon #0" = 0
	"Laris #0" = 0
	"Larza #0" = 0
	"Limer #0" = 0
	"Lykidas #0" = 0
	"Marod #0" = 0
	"Marodeen #0" = 0
	"Mehran #0" = 0
	"Mesten #0" = 0
	"Nahroon #0" = 0
	"Naram #0" = 0
	"Numair #0" = 0
	"Oktan #0" = 0
	"Oreias #0" = 0
	"Panthes #0" = 0
	"Raafi #0" = 0
	"Rabon #0" = 0
	"Radin #0" = 0
	"Rimush #0" = 0
	"Saamir #0" = 0
	"Sabum #0" = 0
	"Saed #0" = 0
	"Samiun #0" = 0
	"Sargin #0" = 0
	"Sepeh #0" = 0
	"Serim #0" = 0
	"Sharaf #0" = 0
	"Suhlamu #0" = 0
	"Temenos #0" = 0
	"Theron #0" = 0
	"Tozan #0" = 0
	"Tydeos #0" = 0
	"Udish #0" = 0
	"Ufuk #0" = 0
	"Yazkur #0" = 0
	"Zaid #0" = 0
	"Zamman #0" = 0
	"Zimud #0" = 0
	"Zimudar #0" = 0
	
	"Aamina #0" = -1
	"Abella #0" = -1
	"Amata #0" = -1
	"Amina #0" = -1
	"Anatu #0" = -1
	"Aqeela #0" = -1
	"Araha #0" = -1
	"Arahuna #0" = -1
	"Arwia #0" = -1
	"Asiah #0" = -1
	"Baila #0" = -1
	"Benan #0" = -1
	"Canfeda #0" = -1
	"Damrina #0" = -1
	"Derya #0" = -1
	"Dilara #0" = -1
	"Elif #0" = -1
	"Elina #0" = -1
	"Elmas #0" = -1
	"Emmita #0" = -1
	"Ettu #0" = -1
	"Fatma #0" = -1
	"Gula #0" = -1
	"Haala #0" = -1
	"Haamida #0" = -1
	"Hasibe #0" = -1
	"Ia #0" = -1
	"Iltani #0" = -1
	"Ishtara #0" = -1
	"Jannat #0" = -1
	"Kamelya #0" = -1
	"Kishar #0" = -1
	"Kullaa #0" = -1
	"Leja #0" = -1
	"Lubna #0" = -1
	"Mardina #0" = -1
	"Mudam #0" = -1
	"Musheera #0" = -1
	"Nadide #0" = -1
	"Nahrina #0" = -1
	"Nardina #0" = -1
	"Nesrin #0" = -1
	"Ninsunu #0" = -1
	"Nuriye #0" = -1
	"Nusaiba #0" = -1
	"Nuzha #0" = -1
	"Omara #0" = -1
	"Pari #0" = -1
	"Qamara #0" = -1
	"Rahma #0" = -1
	"Saba #0" = -1
	"Sabahat #0" = -1
	"Samra #0" = -1
	"Semiha #0" = -1
	"Seyma #0" = -1
	"Sireen #0" = -1
	"Ura #0" = -1
	"Yamima #0" = -1
	"Yamina #0" = -1
	"Zakiti #0" = -1
	"Zeyni #0" = -1
	
}

leader_names = {

	
	#Generic Harpy
	Shrieker Feathersworn Blackfeather Bluefeather Redfeather Greenfeather Talonheart Talonbeak Ladybreaker Maneater Manbreaker Manchewer Mantaker Malesworn Clawbeak Clawfeather Clawtalon
	Cronemother Cronebeak Cronefeather Bluewing Goldwing Goldclaw Goldtalon Goldeye Redeye "One-eye" Tallpeak Tallwind Windpeak Windtalon Windwing Birdsworn Birdlady
	Screechsong Birdsong Soothesong Windsong Beaksong Melodysong Songshriek Songfeather Songbeak Songpeaks
	
	#Titles
	"'the Banshee'" "'the Shrieker'" "'the Beautiful'" "'the Ugly'" "'the Crone'" "'the Harpy'" "'the Fury'" "'the Singer'" "'the Melodious'" "'the Enchantress'"
}

ship_names = {
	#Generic Bulwari
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear "Black Galley" "Black Pinnace" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite Zealous
	
	Akal Flame Fire Blaze Burn "Burning Soul" Elephant

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor

	Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
}	

army_names = {
	"Harpy Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Harpy Fleet" "Sun Squadron" "Flame Squadron" "Diven Squadron" "Glass Squadron" "Mirrors Squadron" "Echoes Squadron"
}