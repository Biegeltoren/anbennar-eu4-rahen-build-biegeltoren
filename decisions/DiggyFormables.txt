country_decisions = {

	I01_nation = {
		major = yes
		potential = {
			owns = 4119 #Amldihr
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I01 }
		}
		allow = {
			owns_core_province = 4119
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I01
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = kronium_dwarf
			}
			change_primary_culture = kronium_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I02_nation = {
		major = yes
		potential = {
			owns = 4208 
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I02 }
		}
		allow = {
			owns_core_province = 4208
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I02
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = quartz_dwarf
			}
			change_primary_culture = quartz_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I03_nation = {
		major = yes
		potential = {
			owns = 4020 
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I03 }
		}
		allow = {
			owns_core_province = 4020
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I03
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = sapphire_dwarf
			}
			change_primary_culture = sapphire_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I04_nation = {
		major = yes
		potential = {
			owns = 4147 
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I04 }
		}
		allow = {
			owns_core_province = 4147
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I04
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = ramsteel_dwarf
			}
			change_primary_culture = ramsteel_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I05_nation = {
		major = yes
		potential = {
			owns = 4236 
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I05 }
		}
		allow = {
			owns_core_province = 4236
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I05
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = mithril_dwarf
			}
			change_primary_culture = mithril_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I06_nation = {
		major = yes
		potential = {
			owns = 2931 #Ernatvir
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I06 }
		}
		allow = {
			owns_core_province = 2931
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I06
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = dagrite_dwarf
			}
			change_primary_culture = dagrite_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I07_nation = {
		major = yes
		potential = {
			owns = 2863
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I07 }
		}
		allow = {
			owns_core_province = 2863
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I07
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = agate_dwarf
			}
			change_primary_culture = agate_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I08_nation = {
		major = yes
		potential = {
			owns = 2868 #Asra Hold
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I08 }
		}
		allow = {
			owns_core_province = 2868
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I08
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = platinum_dwarf
			}
			change_primary_culture = platinum_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I09_nation = {
		major = yes
		potential = {
			owns = 4056
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I09 }
		}
		allow = {
			owns_core_province = 4056
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I09
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = opal_dwarf
			}
			change_primary_culture = opal_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I10_nation = {
		major = yes
		potential = {
			owns = 2963
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I10 }
		}
		allow = {
			owns_core_province = 2963
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I10
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = lead_dwarf
			}
			change_primary_culture = lead_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I11_nation = {
		major = yes
		potential = {
			owns = 2942
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I11 }
		}
		allow = {
			owns_core_province = 2942
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I11
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = marble_dwarf
			}
			change_primary_culture = marble_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I12_nation = {
		major = yes
		potential = {
			owns = 4238
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I12 }
		}
		allow = {
			owns_core_province = 4238
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I12
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = diamond_dwarf
			}
			change_primary_culture = diamond_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I13_nation = {
		major = yes
		potential = {
			owns = 4100
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I13 }
		}
		allow = {
			owns_core_province = 4100
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I13
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = flint_dwarf
			}
			change_primary_culture = flint_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I14_nation = {
		major = yes
		potential = {
			owns = 4217
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I14 }
		}
		allow = {
			owns_core_province = 4217
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I14
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = titanium_dwarf
			}
			change_primary_culture = titanium_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I15_nation = {
		major = yes
		potential = {
			owns = 4078
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I15 }
		}
		allow = {
			owns_core_province = 4078
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I15
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = pyrite_dwarf
			}
			change_primary_culture = pyrite_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I16_nation = {
		major = yes
		potential = {
			owns = 4039
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I16 }
		}
		allow = {
			owns_core_province = 4039
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I16
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = basalt_dwarf
			}
			change_primary_culture = basalt_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I17_nation = {
		major = yes
		potential = {
			owns = 4218
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I17 }
		}
		allow = {
			owns_core_province = 4218
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I17
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = topaz_dwarf
			}
			change_primary_culture = topaz_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I18_nation = {
		major = yes
		potential = {
			owns = 4265
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I18 }
		}
		allow = {
			owns_core_province = 4265
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I18
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = peridot_dwarf
			}
			change_primary_culture = peridot_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I19_nation = {
		major = yes
		potential = {
			owns = 4284
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I18 }
		}
		allow = {
			owns_core_province = 4284
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I19
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = granite_dwarf
			}
			change_primary_culture = granite_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I20_nation = {
		major = yes
		potential = {
			owns = 4266
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I20 }
		}
		allow = {
			owns_core_province = 4266
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I20
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = amethyst_dwarf
			}
			change_primary_culture = amethyst_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I21_nation = {
		major = yes
		potential = {
			owns = 4350
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I21 }
		}
		allow = {
			owns_core_province = 4350
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I21
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = emerald_dwarf
			}
			change_primary_culture = emerald_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I22_nation = {
		major = yes
		potential = {
			owns = 4311
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I22 }
		}
		allow = {
			owns_core_province = 4311
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I22
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = jade_dwarf
			}
			change_primary_culture = jade_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	I23_nation = {
			major = yes
			potential = {
				tag = H94
			}
			allow = {
				is_free_or_tributary_trigger = yes
				is_nomad = no
				is_at_war = no				
				num_of_owned_provinces_with = {
					value = 10
					
					is_core = yes
					is_city = yes
					OR = {
						has_terrain = dwarven_hold
						has_terrain = dwarven_hold_surface
					}
				}
			}
			effect = {
				change_tag = I23
				swap_non_generic_missions = yes
				remove_non_electors_emperors_from_empire_effect = yes

				add_prestige = 25
				if = {
					limit = { has_custom_ideas = no }
					country_event = { id = ideagroups.1 } #Swap Ideas
				}
				change_government = republic
				add_government_reform = obsidian_council_reform
			}
			ai_will_do = {
				factor = 1
			}
			ai_importance = 400
		}
	I24_nation = {
		major = yes
		potential = {
			has_country_modifier = dwarven_administration
			culture_group = dwarven
			capital_scope = { continent = serpentspine }
			NOT = { exists = I24 }
			NOT = { tag = I23 }
			NOT = { tag = H94 }
			NOT = { has_reform = adventurer_reform }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no				
			num_of_owned_provinces_with = {
				value = 25
				
				is_core = ROOT
				is_city = yes
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
			}
			owns_core_province = 4119
		}
		provinces_to_highlight = {
			OR = {
				has_terrain = dwarven_hold
				has_terrain = dwarven_hold_surface
			}
			continent = serpentspine
		}
		effect = {
			change_tag = I24
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			4119 = { move_capital_effect = yes }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	I25_nation = {
		major = yes
		potential = {
			owns = 4323
			has_reform = adventurer_reform
			has_country_modifier = dwarven_administration
			NOT = { has_country_flag = formed_diggy_formable }
			NOT = { exists = I25 }
		}
		allow = {
			owns_core_province = 4323
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = I25
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_diggy_formable
			clr_country_flag = knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			every_owned_province = {
				limit = { culture_group = dwarven }
				change_culture = malachite_dwarf
			}
			change_primary_culture = malachite_dwarf
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
}

