
country_decisions = {
	rahen_reform_military = {
		major = yes
	
		potential = {
			capital = 4411
			has_reform = raja_reform
			
			4411 = {
				has_province_modifier = rahen_disloyal_officers
			}
		}
		
		allow = {
			NOT = { has_country_modifier = rahen_court_reformations }
			if = {
				limit = { ai = yes }
				stability = 0
			}
			else = { stability = 2 }
			OR = {
				mil = 5
				army_reformer = 2
			}
		}
	
		effect = {
			if = {
				limit = { ai = no }
				add_stability = -2
			}
			add_country_modifier = {
				name = rahen_court_reformations
				duration = -1
			}
			country_event = {
				id = rahenraj.7
				days = 2
				random = 10
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	rahen_reform_ministry = {
		major = yes
	
		potential = {
			capital = 4411
			has_reform = raja_reform
			
			4411 = {
				has_province_modifier = rahen_corrupt_ministers
			}
		}
		
		allow = {
			NOT = { has_country_modifier = rahen_court_reformations }
			if = {
				limit = { ai = yes }
				stability = 0
			}
			else = { stability = 2 }
			OR = {
				adm = 5
				inquisitor = 2
			}
		}
	
		effect = {
			if = {
				limit = { ai = no }
				add_stability = -2
			}
			add_country_modifier = {
				name = rahen_court_reformations
				duration = -1
			}
			country_event = {
				id = rahenraj.20
				days = 2
				random = 10
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	rahen_reform_foreign_office = {
		major = yes
	
		potential = {
			capital = 4411
			has_reform = raja_reform
			
			4411 = {
				has_province_modifier = rahen_arrogant_foreign_office
			}
		}
		
		allow = {
			NOT = { has_country_modifier = rahen_court_reformations }
			if = {
				limit = { ai = yes }
				stability = 0
			}
			else = { stability = 2 }
			OR = {
				dip = 5
				statesman = 2
			}
			is_year = 1490
		}
	
		effect = {
			if = {
				limit = { ai = no }
				add_stability = -2
			}
			add_country_modifier = {
				name = rahen_court_reformations
				duration = -1
			}
			country_event = {
				id = rahenraj.40
				days = 2
				random = 10
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	rahen_reform_nobility = {
		major = yes
	
		potential = {
			capital = 4411
			has_reform = raja_reform
			
			4411 = {
				has_province_modifier = rahen_powerful_nobility
			}
		}
		
		allow = {
			NOT = { has_country_modifier = rahen_court_reformations }
			if = {
				limit = { ai = yes }
				stability = 0
			}
			else = { stability = 2 }
			OR = {
				mil = 5
				grand_captain = 2
			}
		}
	
		effect = {
			if = {
				limit = { ai = no }
				add_stability = -2
			}
			add_country_modifier = {
				name = rahen_court_reformations
				duration = -1
			}
			country_event = {
				id = rahenraj.50
				days = 2
				random = 10
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}