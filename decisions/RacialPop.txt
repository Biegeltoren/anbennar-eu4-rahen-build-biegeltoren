country_decisions = {
	what_is_my_current_tolerance = {
		major = yes
		potential = {
			ai = no
			#has_country_modifier = setup_for_races_modifier
		}
		allow = {
			custom_trigger_tooltip = {
				tooltip = tolerance_of_elven_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_halfling_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_dwarven_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_gnomish_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_orcish_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_kobold_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_half_orcish_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_human_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_gnollish_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_ruinborn_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_goblin_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_harpy_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_troll_info_tooltip
				always = no
			}
			custom_trigger_tooltip = {
				tooltip = tolerance_of_half_elven_info_tooltip
				always = no
			}
		}
		effect = {
			add_treasury = 1	# You won't have access to anything (will later change to tooltip)
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	
	racial_pop_menu = {
		major = yes
		potential = {

		}
		allow = {
			
		}
		effect = {
			country_event = { 
				id = racial_pop_misc.1
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { has_low_tolerance_minority_trigger = yes }
			}
			modifier = {
				factor = 0
				NOT = { adm_power = 100 }
			}
			modifier = {
				factor = 0
				has_country_modifier = racial_pop_menu_cooldown
			}
			# modifier = {
				# factor = 0
				# NOT = { stability = 2 }
			# }
		}
	}
}
