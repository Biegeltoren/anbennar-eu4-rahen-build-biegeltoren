#550 - Akal'telger

owner = F22
controller = F22
add_core = F22
add_core = F21
culture = bahari
religion = bulwari_sun_cult

base_tax = 6
base_production = 7
base_manpower = 4

trade_goods = cloth

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari


add_permanent_province_modifier = {
	name = elven_minority_integrated_large
	duration = -1
}