culture = wood_elf
religion = regent_court
hre = no
is_city = yes
trade_goods = unknown
base_tax = 1
base_manpower = 1
base_production = 1
native_size = 0
native_ferocity = 10
native_hostileness = 10

discovered_by = tech_wood_elf