#260 - Lublin

owner = A94
controller = A94
add_core = A94
culture = esmari
religion = regent_court

hre = yes

base_tax = 8
base_production = 8
base_manpower = 6

trade_goods = cloth
capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

add_permanent_province_modifier = {
	name = half_elven_minority_coexisting_large
	duration = -1
}