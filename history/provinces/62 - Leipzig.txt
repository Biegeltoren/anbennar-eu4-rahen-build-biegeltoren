#62 - Leipzig | 

owner = A20
controller = A20
add_core = A20
culture = ruby_dwarf
religion = ancestor_worship

hre = no

base_tax = 11
base_production = 11
base_manpower = 9

trade_goods = gems
capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
add_permanent_province_modifier = {
	name = dig_2
	duration = -1
}