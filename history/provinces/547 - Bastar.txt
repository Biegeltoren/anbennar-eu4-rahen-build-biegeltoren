#547 - Tremo'enkost

owner = F22
controller = F22
add_core = F22
add_core = F21
culture = sun_elf
religion = bulwari_sun_cult

base_tax = 3
base_production = 3
base_manpower = 2

trade_goods = grain
center_of_trade = 1

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = human_minority_coexisting_large
	duration = -1
}
