# No previous file for Banten
owner = F46
controller = F46
add_core = F46
culture = desert_elf
religion = the_jadd
center_of_trade = 1

hre = no

base_tax = 3
base_production = 4
base_manpower = 2

trade_goods = spices

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari