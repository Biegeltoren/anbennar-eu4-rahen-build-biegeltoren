# No previous file for Ha Tinh
owner = G05
controller = G05
add_core = G05
culture = kwineh
religion = weeping_mother
capital = ""

hre = no

base_tax = 3
base_production = 4
base_manpower = 3

trade_goods = unknown

native_size = 18
native_ferocity = 3
native_hostileness = 2

add_permanent_province_modifier = {
	name = sella_estuary_modifier
	duration = -1
}