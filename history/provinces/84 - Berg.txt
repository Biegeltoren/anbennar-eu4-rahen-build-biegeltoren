#84 - Berg | 

owner = A18
controller = A18
add_core = A18
add_core = A16
culture = sorncosti
religion = regent_court

hre = no

base_tax = 6
base_production = 6
base_manpower = 4

trade_goods = wine

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
