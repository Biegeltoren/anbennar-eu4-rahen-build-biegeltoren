# No previous file for Arikara
owner = H01
controller = H01
add_core = H01
culture = selphereg
religion = eordellon
capital = "Arakeprun"
fort_15th = yes
center_of_trade = 2

hre = no

base_tax = 8
base_production = 5
base_manpower = 4

trade_goods = incense

native_size = 14
native_ferocity = 6
native_hostileness = 6

add_permanent_province_modifier = {
	name = arrag_estuary_modifier
	duration = -1
}
