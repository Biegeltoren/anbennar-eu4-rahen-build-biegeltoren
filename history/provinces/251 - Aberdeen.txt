#251 - Aberdeen

owner = A23
controller = A23
add_core = A23
culture = vertesker
religion = regent_court

hre = yes

base_tax = 7
base_production = 7
base_manpower = 5

trade_goods = fish

capital = ""

is_city = yes
fort_15th = yes 


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

