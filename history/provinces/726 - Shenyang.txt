#726

owner = B45
controller = B45
add_core = B45
culture = common_goblin
religion = goblinic_shamanism


base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = naval_supplies

capital = ""

is_city = yes

native_size = 47
native_ferocity = 5
native_hostileness = 8

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari