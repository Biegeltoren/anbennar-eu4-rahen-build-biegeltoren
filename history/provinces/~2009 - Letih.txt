# No previous file for Letih
owner = H04
controller = H04
add_core = H04
culture = selphereg
religion = eordellon
capital = "Letih"

hre = no

base_tax = 3
base_production = 2
base_manpower = 2

trade_goods = fish

native_size = 14
native_ferocity = 6
native_hostileness = 6