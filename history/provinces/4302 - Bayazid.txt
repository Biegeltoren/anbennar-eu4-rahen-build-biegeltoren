owner = R62
controller = R62
add_core = R62
culture = brown_orc
religion = great_dookan
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
add_permanent_province_modifier = {
	name = dwarovar_rail
	duration = -1
}
add_permanent_province_modifier = {
	name = haraz_path
	duration = -1
}