#438 - Kara Kum

owner = A59
controller = A59
add_core = A59
culture = corvurian
religion = regent_court

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = wool

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

