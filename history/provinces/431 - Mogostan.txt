#431 - Mogostan

owner = A60
controller = A60
add_core = A60
culture = corvurian
religion = regent_court

hre = no

base_tax = 4
base_production = 5
base_manpower = 4

trade_goods = gems
center_of_trade = 1

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

