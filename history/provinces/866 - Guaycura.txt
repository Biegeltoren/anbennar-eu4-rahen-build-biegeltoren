# No previous file for Guaycura

owner = B55
controller = B55
add_core = B55
culture = castellyrian
religion = regent_court

hre = no

base_tax = 2
base_production = 3
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

add_permanent_province_modifier = {
	name = white_walls_of_castanor
	duration = -1
}