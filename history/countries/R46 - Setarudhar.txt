government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = dhukharuved
religion = high_philosophy
technology_group = tech_raheni
religious_school = orange_sash_school
capital = 4425

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Raghudev"
		dynasty = "i Setarudhar"
		birth_date = 1410.11.3
		adm = 4
		dip = 5
		mil = 2
		culture = west_harimari
	}
}