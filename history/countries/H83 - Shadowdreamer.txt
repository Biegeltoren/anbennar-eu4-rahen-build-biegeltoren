government = tribal
add_government_reform = dwarovar_warband
government_rank = 1
primary_culture = black_orc
religion = great_dookan
technology_group = tech_orcish
capital = 754

1442.3.17 = {
	monarch = {
		name = "Bigtooth"
		dynasty = "Shadoworb"
		birth_date = 1382.11.4
		adm = 1
		dip = 1
		mil = 1
	}
}