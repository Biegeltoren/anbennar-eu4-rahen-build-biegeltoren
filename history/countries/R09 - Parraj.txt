government = monarchy
add_government_reform = autocracy_reform
government_rank = 1
primary_culture = rajnadhid
religion = high_philosophy
technology_group = tech_raheni
religious_school = silk_turban_school
capital = 4466

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Maurayadhwaj"
		dynasty = "i Parraj"
		birth_date = 1420.6.8
		adm = 5
		dip = 4
		mil = 6
		culture = west_harimari
	}
}