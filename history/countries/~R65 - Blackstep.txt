government = tribal
add_government_reform = slave_state
government_rank = 1
primary_culture = jungle_goblin
religion = goblinic_shamanism
technology_group = tech_goblin
national_focus = DIP
capital = 733

1430.1.1 = {
	monarch = {
		name = "Naum"
		dynasty = "Greenshade"
		birth_date = 1410.10.4
		adm = 4
		dip = 4
		mil = 2
	}
}