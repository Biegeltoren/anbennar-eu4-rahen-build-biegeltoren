government = monarchy
add_government_reform = adventurer_reform
government_rank = 1
primary_culture = silver_dwarf
religion = ancestor_worship
technology_group = tech_dwarven
capital = 2931

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1442.3.17 = {
	monarch = {
		name = "Khuznoli"
		dynasty = "Cavebrow"
		birth_date = 1410.11.4
		adm = 3
		dip = 4
		mil = 4
	}
	add_ruler_personality = immortal_personality
	add_historical_friend = A73
}