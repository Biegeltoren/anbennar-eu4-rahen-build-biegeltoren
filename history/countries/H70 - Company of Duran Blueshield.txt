government = monarchy
add_government_reform = adventurer_reform
government_rank = 1
primary_culture = iron_dwarf
religion = ancestor_worship
technology_group = tech_dwarven
capital = 2931

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }


1442.3.17 = {
	monarch = {
		name = "Duran"
		dynasty = "Blueshield"
		birth_date = 1290.11.4
		adm = 6
		dip = 5
		mil = 3
	}
	add_ruler_personality = immortal_personality
}