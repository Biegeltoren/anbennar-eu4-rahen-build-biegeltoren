government = republic
add_government_reform = free_slave_reform
government_rank = 2
primary_culture = sarniryabsad
add_accepted_culture = raghamidesh
add_accepted_culture = dhukharuved
religion = high_philosophy
technology_group = tech_raheni
religious_school = radiant_sun_school
capital = 4420
historical_rival = R51

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }