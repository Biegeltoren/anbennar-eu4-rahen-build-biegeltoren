government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = rabhidarubsad
religion = high_philosophy
technology_group = tech_raheni
religious_school = golden_palace_school
capital = 4415

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Mahindranarayan"
		dynasty = "Suran"
		birth_date = 1419.10.7
		adm = 2
		dip = 5
		mil = 3
		culture = west_harimari
	}
}