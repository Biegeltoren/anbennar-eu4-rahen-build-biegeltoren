government = republic
add_government_reform = merchants_reform
government_rank = 1
primary_culture = olavish
religion = skaldhyrric_faith
technology_group = tech_cannorian
capital = 975
national_focus = DIP

1443.2.10 = {
	monarch = {
		name = "Steinar of Redgarhavn"
		birth_date = 1401.3.3
		adm = 4
		dip = 2
		mil = 1
	}
}
1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }