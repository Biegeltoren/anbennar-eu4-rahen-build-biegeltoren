government = tribal
add_government_reform = dwarovar_warband
government_rank = 1
primary_culture = black_orc
religion = great_dookan
technology_group = tech_orcish
capital = 754

1442.3.17 = {
	monarch = {
		name = "Kandoul"
		dynasty = "Madmax"
		birth_date = 1382.11.4
		adm = 4
		dip = 1
		mil = 6
	}
}