
nimscodd_dragon_coast = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = A06
	}
	has_country_shield = yes
	
	nimscodd_gather_forces = {
		icon = mission_build_up_to_force_limit
		required_missions = { }
		position = 1
		
		trigger = {
			army_size_percentage = 1
		}
		
		effect = {
			gnomish_pass_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_country_modifier = {
				name = "thriving_arms_industry"
				duration = 9125 #25 years
			}
		}
	}
	
	nimscodd_gnomish_pass = {
		icon = mission_assemble_an_army
		required_missions = { nimscodd_gather_forces }
		position = 2

		provinces_to_highlight = {
			OR = {
				area = gnomish_pass_area
			}
			NOT = {
				owner = { 
					NOT = { 
						primary_culture = redscale_kobold
						primary_culture = bluescale_kobold
						primary_culture = greenscale_kobold
						}
				}
			}
		}
		trigger = {
			170 = {
				owner = { 
					NOT = { 
						primary_culture = redscale_kobold
						primary_culture = bluescale_kobold
						primary_culture = greenscale_kobold
						}
				}
			}
			167 = {
				owner = { 
					NOT = { 
						primary_culture = redscale_kobold
						primary_culture = bluescale_kobold
						primary_culture = greenscale_kobold
						}
				}
			}
			145 = {
				owner = { 
					NOT = { 
						primary_culture = redscale_kobold
						primary_culture = bluescale_kobold
						primary_culture = greenscale_kobold
						}
				}
			}
			148 = {
				owner = { 
					NOT = { 
						primary_culture = redscale_kobold
						primary_culture = bluescale_kobold
						primary_culture = greenscale_kobold
						}
				}
			}
			144 = {
				owner = { 
					NOT = { 
						primary_culture = redscale_kobold
						primary_culture = bluescale_kobold
						primary_culture = greenscale_kobold
						}
				}
			}
		}
		effect = {
			dragondowns_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	nimscodd_continue_the_push = {
		icon = mission_cannons_firing
		required_missions = { nimscodd_gnomish_pass }
		position = 3

		provinces_to_highlight = {
			OR = {
				area = dragondowns_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			dragondowns_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			dragonpoint_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	nimscodd_reclaim_old_capital = {
		icon = mission_unite_home_region
		required_missions = { nimscodd_continue_the_push }
		position = 4
		
		provinces_to_highlight = {
			OR = {
				province_id = 177
				province_id = 167
				province_id = 172
				province_id = 173
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			owns_or_non_sovereign_subject_of = 177
			owns_or_non_sovereign_subject_of = 167
			owns_or_non_sovereign_subject_of = 172
			owns_or_non_sovereign_subject_of = 173
		}
		effect = {
			dragon_coast_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			177 = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1
				change_culture = ROOT
				change_religion = ROOT
			}
			country_event = { id = gnomes.1 }
		}
	}
	
	nimscodd_rebuild_old_capital = {
		icon = mission_unite_home_region
		required_missions = { nimscodd_reclaim_old_capital }
		position = 5
		
		provinces_to_highlight = {
			province_id = 177
		}
		trigger = {
			owns = 177
			177 =  {development = 25}
		}
		effect = {
			177 = { change_trade_goods = gems }
		}
	}

	nimscodd_revitalize_old_industries = {
		icon = mission_unite_home_region
		required_missions = {
			nimscodd_rebuild_old_capital
			nimscodd_build_up_the_infrastructure
		}
		position = 6
		
		trigger = {
			num_of_owned_provinces_with = {
				value = 8
				development = 15
				OR = {
					trade_goods = fish
					trade_goods = wool
				}
				has_building = workshop
				OR = {
					area = dragonpoint_area
					area = dragonheights_area
					area = gnomish_pass_area
					area = dragondowns_area
				}
			}
		}
		effect = {
			every_owned_province = {
				limit = {
					development = 15
					OR = {
						trade_goods = fish
						trade_goods = wool
					}
					has_building = workshop
					OR = {
						area = dragonpoint_area
						area = dragonheights_area
						area = gnomish_pass_area
						area = dragondowns_area
					}
				}
				random_list = {
					1 = { change_trade_goods = gems }
					1 = { change_trade_goods = glass }
					1 = { change_trade_goods = cloth }
				}
			}
		}
	}
	
	nimscodd_gnomish_luxury_goods = {
		icon = mission_war_chest
		required_missions = {
			nimscodd_revitalize_old_industries
			nimscodd_take_business_upstream
		}
		position = 8
		
		trigger = {
			production_leader = {
				trade_goods = glass
			}
			production_leader = {
				trade_goods = gems
			}
		}
		effect = {
			177 = {
				add_permanent_province_modifier = {
					name = "gnomish_workshops"
					duration = -1
				}
			}
			random_owned_province = {
				limit = {
					trade_goods = gems
					region = dragon_coast_region
				}
				add_permanent_province_modifier = {
					name = "gnomish_workshops"
					duration = -1
				}
			}
			random_owned_province = {
				limit = {
					trade_goods = glass
					region = dragon_coast_region
				}
				add_permanent_province_modifier = {
					name = "gnomish_workshops"
					duration = -1
				}
			}
			random_owned_province = {
				limit = {
					trade_goods = glass
					region = dragon_coast_region
				}
				add_permanent_province_modifier = {
					name = "gnomish_workshops"
					duration = -1
				}
			}
		}
	}
}

nimscodd_trade = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = A06
	}
	has_country_shield = yes
	
	nimscodd_trade_fleet = {
		icon = mission_galleys_in_port
		required_missions = { }
		position = 1
		
		trigger = {
				num_of_light_ship = 15
			}
			
		effect = {
			add_country_modifier = {
				name = "merchant_navy"
				duration = 9125
			}
		}
	}
	
	nimscodd_build_up_the_infrastructure = {
		icon = mission_hanseatic_city
		required_missions = { 
			nimscodd_trade_fleet
		}
		position = 5
		trigger = {
			marketplace = 5
		}

		effect = {
			169 = {
				add_base_production = 1
			}
			167 = {
				add_base_production = 1
			}
			146 = {
				add_base_production = 1
			}
			145 = {
				add_base_production = 1
			}
		}
	}
	
	nimscodd_show_dominance = {
		icon = mission_sea_battles
		required_missions = { nimscodd_build_up_the_infrastructure }
		position = 6
		
		trigger = {
			1294 = {
				trade_share = {
					country = ROOT
					share = 60
				}
			}
		}

		effect = {
			add_mercantilism = 2
		}
	}
	
	nimscodd_take_business_upstream = {
		icon = mission_protect_white_sea_trade
		required_missions = { nimscodd_show_dominance }
		position = 7
		
		provinces_to_highlight = {
			OR = {
				province_id = 191
				province_id = 696
				province_id = 956
				province_id = 959
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			191 = { owned_by = ROOT }
			696 = { owned_by = ROOT }
			956 = { owned_by = ROOT }
			959 = { owned_by = ROOT }
		}

		effect = {
			191 = {
				add_base_production = 1
			}
			696 = {
				add_base_production = 1
			}
			956 = {
				add_base_production = 1
			}
			959 = {
				add_base_production = 1
			}
		}
	}
}


nimscodd_friendship = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = A06
	}
	has_country_shield = yes
	
	nimscodd_reach_out_to_our_cousins = {
		icon = mission_alliances
		required_missions = { }
		position = 1
		
		trigger = {
			A81 = {
				has_opinion = {
					who = ROOT
					value = 120
				}
			}
			A19 = {
				has_opinion = {
					who = ROOT
					value = 120
				}
			}
		}
			
		effect = {
			add_prestige = 5
			add_dip_power = 20
		}
	}
	
	nimscodd_clear_differences = {
		icon = mission_religious
		required_missions = { nimscodd_reach_out_to_our_cousins }
		position = 2
		trigger = {
			has_harmonized_with = cannorian
		}

		effect = {
			add_country_modifier = {
				name = "nimscodd_religious_tolerance"
				duration = -1
			}
		}
	}
	
	nimscodd_reconnect_with_old_friends = {
		icon = mission_noble_council
		required_missions = { nimscodd_clear_differences }
		position = 3
		
		trigger = {
			A97 = {
				exists = yes
				alliance_with = ROOT
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "improved_dip_reputation"
				duration = -1
			}
		}
	}
	
	nimscodd_religion_of_peace = {
		icon = mission_hands_praying
		required_missions = { nimscodd_reconnect_with_old_friends }
		position = 4
		
		trigger = {
			num_of_harmonized = 3
		}
		
		effect = {
			add_country_modifier = {
				name = "nimscodd_peace_religion"
				duration = -1
			}
		}
	}
	
	nimscodd_joint_engineering = {
		icon = mission_alliances
		required_missions = { 
			nimscodd_gnomish_arms_industry
			nimscodd_religion_of_peace
		}
		position = 5
		
		trigger = {
			any_ally = { culture_group = dwarven }
		}
		
		effect = {
			add_country_modifier = {
				name = "nimscodd_joint_dwarven_gnomish_engineering_corps"
				duration = -1
			}
			
			all_ally = {
				limit = {culture_group = dwarven }
				
				add_country_modifier = {
					name = "nimscodd_joint_dwarven_gnomish_engineering_corps"
					duration = -1
				}
			}
		}
	}
	
	nimscodd_design_the_hierarch = {
		icon = mission_establish_high_seas_navy
		required_missions = { 
			nimscodd_new_prototypes
			nimscodd_joint_engineering
		}
		position = 5
		
		trigger = {
			treasury = 500
			mil_power = 150
		}
		
		effect = {
			add_treasury = -500
			add_mil_power = -150
			
			add_country_modifier = {
				name = "nimscodd_the_heirarch_designed"
				duration = -1
			}
			all_country = {
				limit = {
					has_country_modifier = nimscodd_joint_dwarven_gnomish_engineering_corps
					NOT = { tag = ROOT }
				}
				
				add_country_modifier = {
					name = "nimscodd_the_heirarch_designed"
					duration = -1
				}
			}
			custom_tooltip = nimscodd_design_the_heirarch_tt
		}
	}
}


nimscodd_colonization = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = A06
	}
	has_country_shield = yes
	
	nimscodd_looking_for_greener_pastures = {
		icon = mission_colonial
		required_missions = { }
		position = 1
		
		trigger = {
			OR = {
				north_america = { has_discovered = ROOT }
				south_america = { has_discovered = ROOT }
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "colonial_enthusiasm"
				duration = 1825
			}
		}
	}
	
	nimscodd_gnomads = {
		icon = mission_bijapur_karnatak
		required_missions = { nimscodd_looking_for_greener_pastures }
		position = 4
		
		trigger = {
			num_of_owned_provinces_with = {
				value = 2
				OR = {
					continent = north_america
					continent = south_america
				}
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "colonial_enthusiasm"
				duration = 1825
			}
		}
	}
	
	nimscodd_gnomish_trade_empire = {
		icon = mission_trade_company_region_abroad
		required_missions = { 
			nimscodd_gnomads
			nimscodd_show_dominance
		}
		position = 7
		
		trigger = {
			colony = 3
		}
		
		effect = {
			add_country_modifier = {
				name = "nimscodd_the_gnomish_trade_empire"
				duration = -1
			}
		}
	}
}

nimscodd_production_of_arms = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = A06
	}
	has_country_shield = yes
	
	nimscodd_iron_and_brass = {
		icon = mission_have_manufactories
		required_missions = { 
			nimscodd_gnomish_pass
		}
		position = 3
		
		provinces_to_highlight = {
			area = dragonheights_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			dragonheights_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "growing_economy"
				duration = 9125
			}
		}
	}
	
	nimscodd_gnomish_arms_industry = {
		icon = mission_have_manufactories
		required_missions = { 
			nimscodd_iron_and_brass
		}
		position = 4
		
		trigger = {
			weapons = 4
		}
		
		effect = {
			add_country_modifier = {
				name = "production_improvements"
				duration = 9125
			}
		}
	}
	
	nimscodd_new_prototypes = {
		icon = mission_cannons_firing
		required_missions = { 
			nimscodd_gnomish_arms_industry
		}
		position = 5
		
		trigger = {
			innovativeness_ideas = 3
			mil_power = 100
			adm_power = 100
		}
		
		effect = {
			add_mil_power = -100
			add_adm_power = -100
			
			add_country_modifier = {
				name = "nimscodd_new_prototypes_developed"
				duration = -1
			}
			add_estate_influence_modifier = {
				estate = estate_artificers
				desc = "New Prototypes"
				influence = 10
				duration = -1
			}
			custom_tooltip = nimscodd_new_prototypes_tt
		}
	}
	
	
	
	
}